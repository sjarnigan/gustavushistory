﻿using System.Web.Mvc;
using System.Web.Routing;
using gustavusHistory.Controllers;

namespace gustavusHistory
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new {controller = "Home", action = "Index", id = UrlParameter.Optional}
            );
            routes.MapRoute(
                "Archives",
                "Archives/{action}"
            );
            routes.MapRoute(
                "ArchiveItem",
                "Archives/Detail/{id}"
            );
            routes.MapRoute(
                "Articles",
                "Articles/{action}"
            );
            routes.MapRoute(
                "ArticleItem",
                "Articles/Detail/{id}"
            );
            routes.MapRoute(
                "MoreResources",
                "Resources/{action}"
            );
        }
    }
}