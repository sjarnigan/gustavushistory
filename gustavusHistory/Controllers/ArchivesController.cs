using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using gustavusHistory.Models;

namespace gustavusHistory.Controllers
{
    public class ArchivesController : Controller
    {
        // GET
        [OutputCache(CacheProfile = "ListCacheByPage1Month")]
        public ViewResult Photographs(int page = 0, int pageSize = 10)
        {
            var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["main-db"].ConnectionString);
            var results = new Queries.GetArchiveItemsByCategory(page, pageSize, ArchiveCategory.Pho , connection).Query();
            return
            View(results);
        }
        
        // GET
        [OutputCache(CacheProfile = "ListCacheByPage1Month")]
        public ViewResult Documents(int page = 0, int pageSize = 10)
        {
            var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["main-db"].ConnectionString);
            var results = new Queries.GetArchiveItemsByCategory(page, pageSize, ArchiveCategory.Doc , connection).Query();
            return
            View(results);
        }

        // GET
        [OutputCache(CacheProfile = "ListCacheByPage1Month")]
        public ViewResult Artifacts(int page = 0, int pageSize = 10)
        {
            var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["main-db"].ConnectionString);
            var results = new Queries.GetArchiveItemsByCategory(page, pageSize, ArchiveCategory.Art , connection).Query();
            return
                View(results);
        }
        [OutputCache(CacheProfile = "SingleItemById1Month")]
        public ViewResult Detail(int id)
        {
            var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["main-db"].ConnectionString);
            var item = new Queries.GetSingleArchiveItem(id, connection).Query();
            return View(item);
        }
    }
}