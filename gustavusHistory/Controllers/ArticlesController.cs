using System.Configuration;
using System.Data.SqlClient;
using gustavusHistory.Queries;
using System.Web.Mvc;
using gustavusHistory.Models;

namespace gustavusHistory.Controllers
{
    public class ArticlesController : Controller
    {
        // GET
        [OutputCache(CacheProfile = "ListCacheByPage1Month")]
        public ActionResult FactOfTheMatter(int page = 0, int pageSize = 10)
        {    
            var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["main-db"].ConnectionString);
            var items = new Queries.GetArticlesByCategory(page, pageSize, ArticleCategory.Fac, connection).Query();
            return
            View(items);
        }
        [OutputCache(CacheProfile = "ListCacheByPage1Month")]
        public ActionResult Books(int page = 0, int pageSize = 10)
        {
            var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["main-db"].ConnectionString);
            var items = new Queries.GetArticlesByCategory(page, pageSize, ArticleCategory.Boo, connection).Query();
            return
            View(items);
        }
        [OutputCache(CacheProfile = "SingleItemById1Month")]
        public ActionResult Detail(int id)
        {
            var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["main-db"].ConnectionString);
            var article = new Queries.GetSingleArticle(id, connection).Query();
            return
            View(article);
        }
    }
}