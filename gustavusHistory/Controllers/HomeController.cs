﻿using System.Web.Mvc;

namespace gustavusHistory.Controllers
{
    public class HomeController : Controller
    {
        [OutputCache(CacheProfile = "SingleItemById1Month")]
        public ActionResult Index()
        {
            return View();
        }
        [OutputCache(CacheProfile = "SingleItemById1Month")]
        public ActionResult About()
        {
            ViewBag.Message = "SingleItemById1Month.";

            return View();
        }
        [OutputCache(CacheProfile = "SingleItemById1Month")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Here is how to reach us.";

            return View();
        }

        [OutputCache(CacheProfile = "SingleItemById1Month")]
        public ActionResult Donate()
        {
            ViewBag.Message = "Help us preserve our history";

            return View();
        }
    }
}