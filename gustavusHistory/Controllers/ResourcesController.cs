using System.Configuration;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace gustavusHistory.Controllers
{
    public class ResourcesController : Controller
    {
        // GET
        [OutputCache(CacheProfile = "SingleItemById1Month")]
        public ActionResult MoreResources()
        {
            var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["main-db"].ConnectionString);
            var resources = new Queries.GetResources(connection).Query();
            return
            View(resources);
        }
    }
}