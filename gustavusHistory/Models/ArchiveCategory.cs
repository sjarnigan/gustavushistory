using gustavusHistory.Queries;

namespace gustavusHistory.Models
{
    public class ArchiveCategory
    {
        private ArchiveCategory(string value)
        {
            Value = value;
        }

        public string Value { get; set; }

        public static ArchiveCategory Doc => new ArchiveCategory("DOC");
        public static ArchiveCategory Pho => new ArchiveCategory("PHO");
        public static ArchiveCategory Art => new ArchiveCategory("ART");
        
    }
}