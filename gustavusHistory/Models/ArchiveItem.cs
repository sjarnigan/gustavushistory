using System;
using System.Linq;
using System.Web;

namespace gustavusHistory.Models
{
    public class ArchiveItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string Source { get; set; }
        public string Collection { get; set; }
        public string Text { get; set; }
        public int ItemYear { get; set; }
        public string ItemMonth { get; set; }
        public string ItemDay { get; set; }
        public DateTime PostDate { get; set; }
        public string Category { get; set; }
        public string FileName { get; set; }

        public string EstimatedDate()
        {
            var date =string.Empty;
            if (ItemYear == 0) {return date;}
            if (!string.IsNullOrWhiteSpace(ItemDay) && ItemDay != "0") {date += $"{ItemDay}/";}
            if (!string.IsNullOrWhiteSpace(ItemMonth) && ItemDay != "0") {date += $"{ItemMonth}/";}
            date += ItemYear.ToString();

            return date;
        }
    }
}