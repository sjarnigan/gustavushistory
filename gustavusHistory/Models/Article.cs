using System;
using Dapper;

namespace gustavusHistory.Models
{
    public class Article
    {
        public int Id { get; set; }
        public DateTime PublishDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Text { get; set; }
    }
}