namespace gustavusHistory.Models
{
    public class ArticleCategory
    {
        private ArticleCategory(string value)
        {
            Value = value;
        }

        public string Value { get; set; }

        public static ArticleCategory Boo => new ArticleCategory("BOO");
        public static ArticleCategory Fac => new ArticleCategory("FAC");
    }
}