using System.Collections.Generic;
using System.Web;

namespace gustavusHistory.Models
{
    public class PagedResults<T>
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        
        public int PageCount { get; set; }
        public IEnumerable<T> Results { get; set; }
    }
}