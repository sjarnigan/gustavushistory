using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using gustavusHistory.Models;

namespace gustavusHistory.Queries
{
    public class GetArchiveItemsByCategory : IQuery<PagedResults<ArchiveItem>>
    {
        private SqlConnection Connection { get; set; }

        private string sql =
            @"select itemId as Id , title as Title, description as Description, category as Category, Archives.postdate as PostDate,
        Archives.itemtext as Text,Sources.sourcename as Source , Pictures.filename as FileName, Collections.collname as Collection, itemday as ItemDay, itemmonth as ItemMonth, itemyear as ItemYear
from Archives
INNER JOIN Collections on Archives.collid = Collections.collid
INNER JOIN Pictures on Archives.picid = Pictures.picid
INNER JOIN Sources on Archives.sourceid = Sources.sourceid
where Archives.category = @Category
order by postdate
offset (@PageSize * @Page) rows
fetch next @PageSize ROWS only";

        private string countSql =
            @"select distinct itemId from Archives where category = @Category ";

        private int Page { get; set; }

        private int PageSize { get; set; }

        private ArchiveCategory Category { get; set; }

        public GetArchiveItemsByCategory(int page, int pageSize, ArchiveCategory category, SqlConnection connection)
        {
            this.Page = page;
            this.PageSize = pageSize;
            Connection = connection;
            Category = category;
        }

        private int GetCount()
        {
            var results = Connection.Query<int>(countSql, new {Category = this.Category.Value});
            return results.Count();
        }

        public PagedResults<ArchiveItem> Query()
        {
            var pagedResults = new PagedResults<ArchiveItem>
            {
                Page = this.Page,
                PageSize = this.PageSize,
            };
            var param = new DynamicParameters();
            param.Add("Page", Page);
            param.Add("PageSize", PageSize);
            param.Add("Category", Category.Value);
            pagedResults.Results = Connection.Query<ArchiveItem>(sql, param);
            pagedResults.TotalCount = pagedResults.Results.Count();
            pagedResults.PageCount = pagedResults.TotalCount % pagedResults.PageSize != 0 
                ? (pagedResults.TotalCount / pagedResults.PageSize) + 1
                : (pagedResults.TotalCount / pagedResults.PageSize);
            return pagedResults;
        }
    }
}
 