using System.Data.SqlClient;
using System.Linq;
using Dapper;
using gustavusHistory.Models;

namespace gustavusHistory.Queries
{
    public class GetArticlesByCategory : IQuery<PagedResults<Article>>
    {
       private SqlConnection _connection { get; set; }

       private string Sql = @"SELECT title as Title, description as Description, itemtext as Text ,publishdate as PublishedDate, itemid as Id
                            FROM Articles
                            WHERE category = @Category
                            order by postdate
                            offset (@PageSize * @Page) rows
                            fetch next @PageSize ROWS only";
       
       private int Page { get; set; }
       
       private int PageSize { get; set; }
       
       private ArticleCategory _category { get; set; }

       public GetArticlesByCategory(int page, int pageSize, ArticleCategory category, SqlConnection connection)
       {
           Page = page;
           PageSize = pageSize;
           _connection = connection;
           _category = category;
       }
       
       public PagedResults<Article> Query()
       {
           var parameters = new DynamicParameters();
           parameters.Add("Page", Page);
           parameters.Add("PageSize", PageSize);
           parameters.Add("Category", _category.Value);

           var results = new PagedResults<Article>()
           {
               Page = this.Page,
               PageSize = this.PageSize,
           };
           var items = _connection.Query<Article>(Sql, parameters);
           results.Results = items;
           results.TotalCount = results.Results.Count();
           results.PageCount = results.TotalCount % results.PageSize != 0 
               ? (results.TotalCount / results.PageSize) + 1
               : (results.TotalCount / results.PageSize);
           return results;
       }
    }

}