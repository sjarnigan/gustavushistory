using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using gustavusHistory.Models;

namespace gustavusHistory.Queries
{
    public class GetResources: IQuery<IEnumerable<Resource>>
    {
        private string Sql = @"select distinct resid as Id, title as Title, url as Url, description as Description from Resources";
        
        private SqlConnection _connection { get; set; }

        public GetResources(SqlConnection connection)
        {
            _connection = connection;
        }
        
        public IEnumerable<Resource> Query()
        {
            var resources = _connection.Query<Resource>(Sql);
            return resources;
        }
    }
}