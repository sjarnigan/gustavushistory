using System.Data.SqlClient;
using Dapper;
using gustavusHistory.Models;

namespace gustavusHistory.Queries
{
    public class GetSingleArchiveItem: IQuery<ArchiveItem>
    {
        private SqlConnection Connection { get; set; }
        
        private int Id { get; set; }
        
        private static string Sql =             
            @"select itemId as Id , title as Title, description as Description, category as Category, Archives.postdate as PostDate,
            Archives.itemtext as Text, Sources.sourcename as Source , Pictures.filename as FileName, Collections.collname as Collection, itemday as ItemDay, itemmonth as ItemMonth, itemyear as ItemYear
            from Archives
            INNER JOIN Collections on Archives.collid = Collections.collid
            INNER JOIN Pictures on Archives.picid = Pictures.picid
            INNER JOIN Sources on Archives.sourceid = Sources.sourceid
            Where itemId = @Id";

        public GetSingleArchiveItem(int id, SqlConnection connection)
        {
            Id = id;
            Connection = connection;
        }
        
        public ArchiveItem Query()
        {
            var param = new DynamicParameters();
            param.Add("@Id", Id);
            var item = Connection.QueryFirst<ArchiveItem>(Sql, param);
            return item;
        }
    }
}