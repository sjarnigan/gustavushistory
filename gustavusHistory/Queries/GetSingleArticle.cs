using System;
using System.Data.SqlClient;
using Dapper;
using gustavusHistory.Models;

namespace gustavusHistory.Queries
{
    public class GetSingleArticle: IQuery<Article>
    {
        private SqlConnection _connection { get; set; }
        
        private int Id { get; set; }

        private string Sql = @"SELECT itemid as Id, title as Title, description as Description, itemtext as Text ,publishdate as PublishedDate
FROM Articles
WHERE itemid = @Id";

        public GetSingleArticle(int id, SqlConnection connection)
        {
            Id = id;
            _connection = connection;
        }

        public Article Query()
        {
            var param = new DynamicParameters();
            param.Add("@Id", Id);
            var article = _connection.QueryFirst<Article>(Sql, param);

            return article;
        }
    }
}