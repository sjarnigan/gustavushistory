namespace gustavusHistory.Queries
{
    public interface IQuery<out T>
    {
        T Query();
    }
}